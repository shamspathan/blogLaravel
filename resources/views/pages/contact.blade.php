@extends('index')

@section('title','| Contact Me')

@section('content')
<h1>Contact Me </h1>

<hr>

<div class="row">
    <div class="col-md-12">
        <form>

            <div class="form-group">
                <label name="email" > Email: </label>
                <input id="email" name="email" class="form-control">
            </div>

            <div class="form-group">
                <label name="subject" > Subject: </label>
                <input id="subject" name="subject" class="form-control">
            </div>

            <div class="form-group">
                <label name="message" > Message: </label>
                <textarea id="message" name="message" class="form-control"></textarea>
            </div>
            <input type="submit" name="submit" value="Send Message" class="btn btn-success">

        </form>

    </div>

</div>
@endsection

