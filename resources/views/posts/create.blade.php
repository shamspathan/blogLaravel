
@extends('index')

@section('title', "Create Post")

@section('stylesheets')

{!! Html::style('css/parsley.css') !!}

@endsection

@section('content')

<div class="row">
    <div class="col-md-8 col-md-offset-4">
        <h1>Create Post</h1>
        <hr>
        {!! Form::open(['route' => 'posts.store', 'data-parsley-validate' => '']) !!}

        {{ FORM::label('title', 'Title') }}
        {{ FORM::text('title', null, ["class"=>"form-control", 'required' => '', 'maxlength' => '100']) }}

        {{ FORM::label('body', 'Post Body') }}
        {{ FORM::textarea('body', null, ["class"=>"form-control", 'required' => '']) }}

        {{ FORM::submit('Submit Your Post', ["class"=>"btn btn-success btn-lg btn-block", "style" => "margin-top: 20px"]) }}

        {!! Form::close() !!}
        <hr>
    </div>
</div>

@endsection


@section('scripts')

{!! Html::script('js/parsley.min.js') !!}

@endsection
